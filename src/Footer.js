import useTheme from "@material-ui/core/styles/useTheme"
import Container from "@material-ui/core/Container"
import Box from "@material-ui/core/Box"
import Typography from "@material-ui/core/Typography"

export default function Footer() {
  const theme = useTheme()
  const backgroundColor = theme.palette.primary.main

  // this must come from the text of the ApplicationBar or ToolBar
  const textColor = "white" // theme.palette.secondary.main

  return (
    <Container component="footer" maxWidth={false} style={{ backgroundColor: backgroundColor, color: textColor }}>
      <Box display="flex">
        <Box display="flex" flexGrow={1}>
          <Typography variant="caption" color="inherit">
            Актуализирано на 30.04.2021г.
          </Typography>
        </Box>
        <Typography variant="caption" color="inherit">
          Copyright LZ3DY
        </Typography>
      </Box>
    </Container>
  )
}
