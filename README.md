# Bulgarian ham radio tests

This app aims to help amateur radio candidates prepare for class 1 and/or class 2 exams in Bulgaria. The questions for each test category are automatically extracted from the website of [CRC (The Communications Regulation Commission)](https://crc.bg/en).

##### Приложението предлага следните функционалности:
* Избор на радиолюбителски клас
* Избор на раздел или раздели за съответния клас
* Режим подготовка за изпит с възможност за разбъркване на въпросите
* Режим симулиран тест

Приложението съдържа всички възможни въпроси, от които на изпита ще се паднат 60. Продължителността на теста е 40 минути. При 48 и повече верни отговора изпита е успешен.

*********

# Тестове за български радиолюбители

Настоящето приложение има за цел да помогне за успешната подготовка на кандидат-радиолюбителите в клас 1 и/или клас 2 в България. Въпросите за подготвителните тестове са автоматично извлечени от страницата на [КРС (Комисия за регулиране на съобщенията)](https://crc.bg/bg/rubriki/143/testove-za-provejdane-na-izpiti-za-pravosposobnost-na-radiolubiteli).

##### Приложението предлага следните функционалности:
* Избор на радиолюбителски клас
* Избор на раздел или раздели за съответния клас
* Режим подготовка за изпит с възможност за разбъркване на въпросите
* Режим симулиран тест

Приложението съдържа всички възможни въпроси, от които на изпита ще се паднат 60. Продължителността на теста е 40 минути. При 48 и повече верни отговора изпита е успешен.

*********

# How to debug/develop the application

The only dependency needed for the app is [Node.js](https://nodejs.org/en/). If you stick to the latest LTS version you'll be fine. Version 14.17.4 is currently being used for development.

# Start a debug server
Open your terminal and issue the following commands:

* `git clone https://bitbucket.org/batinkov/barts.git # get the latest master`
* `cd barts # get inside the project directory`
* `npm start # start the development server`

In order to access the app open http://localhost:7000/ in your favorite browser. The app works on phones and tablets as well.

# Create a production build
From inside the 'barts' directory created in the previous step issue the following command:

* `npm run build`

This will take a moment and will create a directory called 'build'. This is the production build of the app.

